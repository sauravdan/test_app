var server = require("./server");
var expect = require("expect.js");
var supertest = require("supertest");

describe('test suite', function(){
	it('should expose a function', function(){
		expect(server.app.get).to.be.a('function');
	});
	/*it('should have a key', function(){
		expect(server.app.get).withArgs('/api/search_user/sauravdan').to.have.key('username');
	});*/
});
describe('routing', function(){
	var url = 'http://localhost:3000';
	it('should have a key', function(){
		supertest(url)
			.get('/api/search_user/sauravdan')
			.end(function(error, response){
				if(error)
					throw error;
				expect(response).to.have.key('username');
				done();
			})
	});
});

