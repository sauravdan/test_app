var express = require('express'),
	app = express(),
	morgan = require('morgan'),
	mysql = require('mysql'),
	bodyParser = require("body-parser");
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended:false}));
/*app.get('/api/:name/:age', function(request, response){
	response.json(200, {"hello": request.params.name + " aged " + request.params.age});
});*/
app.listen(3000, function(){
	console.log("ready captain");
});
var pool = mysql.createPool({
	connectionLimit : 100,
	host : 'localhost',
	user : 'root',
	password : 'password',
	database : 'saurav'
});
/*const connect = connection.connect;
const query = connection.query;
const end = connection.end;*/
console.log("About to start get request");
app.get('/api/search_user/:user', function(request, response){
	pool.getConnection(function(error, connection){
		console.log("connection established");
		var user = request.params.user;
		connection.query("SELECT * FROM user WHERE username = ?",[user], function(error, rows, fields){
			connection.release();
			if(!error){
				console.log('The solution is: ', rows);
				response.status(200).json(rows);
				return;
			}
			else{
				console.log('Error while performing query');
				response.status(500).json({error: "failed"});
			}
		});
		connection.on('error', function(err) {
	  		response.json({"code" : 100, "status" : "Error in connection database"});
              		return;
		});
	});
});
app.post('/api/signup_user',function(request,response){
	pool.getConnection(function(error, connection){
		console.log("connection established");
		var username = request.body.user;
		var firstName = request.body.firstName;
		var lastName = request.body.lastName;
		var password = request.body.passwodrd;
		connection.query("INSERT INTO user VALUES(?,?,?,?)",[username,password,firstName,lastName], function(error){
			if(!error)
				{
					var data = {};
					data["Data"] = "Account Created";
					response.status(500).json({error:"Failed"});
					response.end("yes");
				}
			else
				{
					console.log('Error while performing query');
					var data = {};
					data["Data"] = "Account Creation Failed";
					response.status(500).json({error:data});
				}
		});
		connection.on('error', function(error){
			     response.json({"code" : 100, "status" : "Error in connection database"});
			     return;
		});
	});
});
/*app.delete('/api/delete_user/:user', function(request, response){
	pool.getConnection(function(error, connection){
		console.log("connection established");
		var user = request.params.user;
		connection.query("DELETE FROM user WHERE username = ?",[user]
		});
});*/
exports.app = app;
