## Table of Contents
- [Introduction](#introduction)
- [Install](#install)

## Introduction
Web application made using [Express](http://expressjs.com/) at the backend.

## Install
Here's how to get up and running (assuming you have [nodejs](https://nodejs.org/) installed).

```sh
$ git clone https://sauravdan@bitbucket.org/sauravdan/test_app.git
$ npm install
```


